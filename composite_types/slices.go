package compositetypes

import (
	"encoding/json"
	"fmt"
)

func SliceOfString(run bool) {
	if !run {
		return
	}

	t := []byte("string") // convert "string" to slice of bytes.

	fmt.Println("t      : ", t)
	fmt.Println("length : ", len(t))
	fmt.Println("t[2]   : ", t[2])
	fmt.Println("t[:2]  : ", t[:2])
	fmt.Println("t[2:]  : ", t[2:])
	fmt.Println("t[2:2] : ", t[2:2])
	fmt.Println("t[2:3] : ", t[2:3])
	fmt.Println("t[2:3] : ", t[3:5], len(t[3:5]))
	fmt.Println("capacity : ", cap(t))

}

func SliceBasics(run bool) {
	if !run {
		return
	}

	s := []int{1}
	t := []int{1}
	u := make([]int, 1)
	v := make([]int, 1, 2)

	fmt.Printf("s : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(s), cap(s), s, s == nil)
	fmt.Printf("t : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(t), cap(t), t, t == nil)
	fmt.Printf("u : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(u), cap(u), u, u == nil)
	fmt.Printf("v : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(v), cap(v), v, v == nil)

	// invalid operation (slice can only be compared to nil)
	// if s == t {
	// 	fmt.Println("e is equal to f")
	// }

	if s == nil {
		fmt.Println("s is nil slice.")
	} else {
		fmt.Println("s is not a nil slice.")
	}

	var s2 []int            // nil slice   []int(nil)
	s3 := []int{}           // empty slice []int{}
	s4 := make([]int, 0, 5) // slice with length 0 and 5 capcity []int{}
	s5 := make([]int, 1, 5) // slice with length 1 and 5 capcity []int{0}
	s6 := make([]int, 5)    // slice with 5 length and capcity []int{0, 0, 0, 0, 0}

	if s2 == nil {
		fmt.Println("s2 is nil slice.")
	} else {
		fmt.Println("s2 is not a nil slice.")
	}

	if s3 == nil {
		fmt.Println("s3 is nil slice.")
	} else {
		fmt.Println("s3 is not a nil slice. But empty slice.")
	}

	// so best way to check if slcie is epmty (ie, no elment in it) is
	if len(s3) == 0 {
		fmt.Println("s3 is empty. Ie, no elements in it.")
	}

	if len(s4) == 0 {
		fmt.Println("s4 is empty. Ie, no elements in it. Has cap 5 but length is 0.")
	}

	if len(s5) == 0 {
		fmt.Println("s4 is empty. Ie, no elements in it. Has cap 5 but length is 0.")
	} else {
		fmt.Println("s5 is not empty. Has cap 5 but length is 1. One element in it is initilaised default int val 0.")
	}

	if len(s6) == 0 {
		fmt.Println("s6 is empty. Ie, no elements in it. Has cap 5 but length is 0.")
	} else {
		fmt.Println("s6 is not empty. Has cap 5 and length is 5. All elments in it initilaised default int val 0.")
	}
}

func SliceDetailed(run bool) {
	if !run {
		return
	}

	var s []int            // nil slice   []int(nil)
	t := []int{}           // empty slice []int{}
	u := make([]int, 5)    // slice with 5 length and capcity []int{0, 0, 0, 0, 0}
	v := make([]int, 0, 5) // slice with length 0 and 5 capcity []int{}

	fmt.Printf("s : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(s), cap(s), s, s == nil)
	fmt.Printf("t : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(t), cap(t), t, t == nil)
	fmt.Printf("u : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(u), cap(u), u, u == nil)
	fmt.Printf("v : length = %d, capacity = %d, type = %T, is_nil? = %5t, value = %#[3]v \n", len(v), cap(v), v, v == nil)

	fmt.Println()

	j1, _ := json.Marshal(s)
	fmt.Println("json.Marshal(a): ", string(j1))

	j2, _ := json.Marshal(t)
	fmt.Println("json.Marshal(t): ", string(j2))

	j3, _ := json.Marshal(u)
	fmt.Println("json.Marshal(u): ", string(j3))

	j4, _ := json.Marshal(v)
	fmt.Println("json.Marshal(v): ", string(j4))
}

func SliceWTF(run bool) {
	if !run {
		return
	}

	a := [3]int{1, 2, 3} // array of 3 elements

	b := a[:1]  // = a[0:1] slice one element from a
	c := a[0:2] // WTF . it should throw error but it works as undelyign array a have len and cpa 3 (ir 3 elements in it)

	d := a[0:1:1] // take one element from a and set legth to 1
	// d := a[1:1:2] // will not have any elmenti n it  its like a[i:j:k]  d will eb fileld like len = j-i, cap = k-i = 0
	// e := d[0:2]   // not possible as we spacify d has length 1 so we can't take 2 elements from it
	// e := b[0:5]   // also notr possible as undelying array have only 3 elments in it

	a2 := [...]int{1} // array of 1 element

	b2 := a2[:1]
	// c2 := b2[:2] // also not possible as undelaying array a only have 1 elment in it

	fmt.Printf("a  : length = %d, capacity = %d, value = %#v \n", len(a), cap(a), a)
	fmt.Printf("b  : length = %d, capacity = %d, value = %#v \n", len(b), cap(b), b)
	fmt.Printf("c  : length = %d, capacity = %d, value = %#v \n", len(c), cap(c), c)
	fmt.Printf("d  : length = %d, capacity = %d, value = %#v \n", len(d), cap(d), d)
	fmt.Printf("b2 : length = %d, capacity = %d, value = %#v \n", len(b2), cap(b2), b2)

	fmt.Println("\nCampare their adress. Where a, b c and d are stored in memory")
	fmt.Printf("a[%p] = %v\n", &a, a)
	fmt.Printf("b[%p] = %[1]v\n", b)
	fmt.Printf("c[%p] = %[1]v\n", c)
	fmt.Printf("d[%p] = %[1]v\n", d)

	c = append(c, 5)
	c2 := b[0:2:2]
	fmt.Println("\nAfter appending a new elment to c. We changed c a and in that process also changed/mutated a too.")
	fmt.Printf(" a[%p] = %v\n", &a, a)
	fmt.Printf(" b[%p] = %[1]v\n", b)
	fmt.Printf(" c[%p] = %[1]v\n", c)
	fmt.Printf("c2[%p] = %[1]v\n", c2)

	c2 = append(c2, 6)
	fmt.Println("\nAfter appending a new elment to c2.")
	fmt.Printf(" a[%p] = %v\n", &a, a)
	fmt.Printf(" b[%p] = %[1]v\n", b)
	fmt.Printf(" c[%p] = %[1]v\n", c)
	fmt.Printf("c2[%p] = %[1]v\n", c2)

	c[0] = 9
	c2[1] = 8
	fmt.Println("\nChanging vlaues of c and c2.")
	fmt.Printf(" a[%p] = %v\n", &a, a)
	fmt.Printf(" b[%p] = %[1]v\n", b)
	fmt.Printf(" c[%p] = %[1]v\n", c)
	fmt.Printf("c2[%p] = %[1]v\n", c2)
}
