package compositetypes

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func PrintUtf8(run bool) {
	if !run {
		return
	}

	s := "Ꮚ˘ꍓ˘Ꮚ"

	fmt.Printf("%8T %[1]v, %d\n", s, len(s))
	fmt.Printf("%8T %[1]v\n", []rune(s))
	fmt.Printf("%8T %[1]v\n", []byte(s))

}

func WordReplace(run bool) {
	if !run {
		return
	}

	if len(os.Args) < 3 {
		fmt.Fprintln(os.Stderr, "Should provde 3 arguments")
		os.Exit(-1)
		// panic("Should provide 3 args in cms")
	}

	old, new := os.Args[1], os.Args[2]
	scan := bufio.NewScanner(os.Stdin)

	for scan.Scan() {
		s := strings.Split(scan.Text(), old)
		t := strings.Join(s, new)

		fmt.Printf("New string is : %s", t)
	}

}
