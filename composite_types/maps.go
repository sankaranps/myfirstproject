package compositetypes

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"sort"
)

func BasicMaps(run bool) {
	if !run {
		return
	}

	var m map[string]int       // nil map, no storage
	fmt.Println("Map m : ", m) // can read from nil map
	if m == nil {
		fmt.Println("Map m is nil at moment, no storage.")
	}

	a := m["the"]          // can read from nil map
	fmt.Println("a : ", a) // a will be 0, the defualt value for missign key

	// m["the"] = 1   		// can not write to nil map panic: assignment to entry in nil map.

	m2 := make(map[string]int)   // non-nil but empty map
	fmt.Println("Map m2 : ", m2) // can read and
	b := m["the"]                // will be 0
	fmt.Println("b : ", b)

	m2["the"] = 1 // can write to empty map
	fmt.Println("Map m2 : ", m2)

	m = m2

	m["test"] = 1 // ok as m = m2 now
	fmt.Println("Map m : ", m)

	c := m2["test"]
	fmt.Println("c : ", c) // will be 1 as m and m2 poitns to same hash map

	// if m == m2 {
	// 	// invalid operation: cannot compare m == m2 (map can only be compared to nil)
	// }

	if m2 != nil {
		fmt.Println("Can be checked to see if map is nil or not.")
	}

	p := map[string]int{
		"and": 1,
		"the": 2,
	}
	fmt.Println("p : ", p)

	d, ok := p["test"] // here ok means if the key is set or not
	fmt.Printf("d : %d, ok : %v\n", d, ok)

	p["test"]++
	f, ok := p["test"]
	fmt.Printf("f : %d, ok : %v\n", f, ok)

	g := map[string]string{
		"and": "yes",
	}
	h, ok := g["test"]
	fmt.Println("g : ", g)
	fmt.Printf("h : %s, ok : %v\n", h, ok)

	if i, ok := p["sdsd"]; ok {
		fmt.Println("value of key `sdsd` is :", i)
	} else {
		fmt.Println("Default value of key `sdsd` is :", i)
	}
}

func MapTest(run bool) {
	if !run {
		return
	}

	scan := bufio.NewScanner(os.Stdin)
	words := make(map[string]int)

	scan.Split(bufio.ScanWords)

	for scan.Scan() {
		words[scan.Text()]++
	}

	fmt.Println(len(words), "unique words")

	type kv struct {
		key string
		val int
	}

	var ss []kv

	for key, val := range words {
		ss = append(ss, kv{
			key: key,
			val: val,
		})

		// fmt.Println("Key : ", key, " has value : ", val)
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].val > ss[j].val
	})

	for _, s := range ss[:4] {
		fmt.Println(s.key, " appears ", s.val, " times.")
	}

outer:
	for k := range ss {
		for _, v := range ss {
			fmt.Println(k, v)
			continue outer
		}
	}

}

func DifferantMaps(run bool) {
	if !run {
		return
	}
	var s map[string]int         // nil map 			map[string]int(nil)
	t := map[string]int{}        // empty				map[string]int{}
	u := make(map[string]int, 5) // map with 5 length	map[string]int{}
	// v := make(map[string]int, 0, 5) // maps do not have capcity only length

	fmt.Printf("s : length = %d, type = %T, is_nil? = %5t, value = %#[2]v \n", len(s), s, s == nil)
	fmt.Printf("t : length = %d, type = %T, is_nil? = %5t, value = %#[2]v \n", len(t), t, t == nil)
	fmt.Printf("u : length = %d, type = %T, is_nil? = %5t, value = %#[2]v \n", len(u), u, u == nil)

	fmt.Println()

	j1, _ := json.Marshal(s)
	fmt.Println("json.Marshal(a): ", string(j1))

	j2, _ := json.Marshal(t)
	fmt.Println("json.Marshal(t): ", string(j2))

	j3, _ := json.Marshal(u)
	fmt.Println("json.Marshal(u): ", string(j3))
}
