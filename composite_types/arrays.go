package compositetypes

import "fmt"

func Arrybasics(run bool) {
	if !run {
		return
	}

	a := [...]int{}        //  [0]int{}   			len = cap = 0
	b := [...]int{1, 2, 3} // [3]int{1, 2, 3}		len = cap = 3
	c := [4]int{1, 2, 3}   // [4]int{1, 2, 3, 0}	len = cap = 4
	d := [4]int{}          // [4]int{0, 0, 0, 0}	len = cap = 4

	fmt.Printf("a : lenth = %d, capacity = %d, type = %T, value = %#v \n", len(a), cap(a), a, a)
	fmt.Printf("b : lenth = %d, capacity = %d, type = %T, value = %#v \n", len(b), cap(b), b, b)
	fmt.Printf("c : lenth = %d, capacity = %d, type = %T, value = %#v \n", len(c), cap(c), c, c)
	fmt.Printf("d : lenth = %d, capacity = %d, type = %T, value = %#v \n", len(d), cap(d), d, d)

	// a[0] = 3  // not possibel to extend array (immutable. a has len cap = 0)
	b[1] = 4 // possible as b has len/cap 3 and we can change an elment val
	// b[5] = 8 // agian not possible as arry only have cap 3
	d[3] = 8 // ok as it has capacity of 4

	e := [1]int{1}
	f := [1]int{1}
	g := [1]int{2}

	if e == f {
		fmt.Println("e is equal to f")
	}

	if e == g {
		fmt.Println("e is equal to f")
	} else {
		fmt.Println("e is not equal to g")
	}

	// type mismatch (mismatched types [1]int and [0]int)
	// if e == a {
	// 	fmt.Println("e is equal to a")
	// }
}
