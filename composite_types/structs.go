package compositetypes

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type Employee struct {
	name   string
	number int
	Boss   *Employee
	hired  time.Time
}

type Employee2 struct {
	name   string
	number int
	Boss   *Employee
	hired  time.Time
}

func BasicStruct(run bool) {
	if !run {
		return
	}

	var e Employee

	fmt.Println("Before settign values")
	fmt.Printf("E : %T %+[1]v \n", e)
	fmt.Println("")

	e.name = "Mathew"
	e.number++
	e.hired = time.Now()

	fmt.Println("After settign values")
	fmt.Printf("E : %T %+[1]v \n", e)
	fmt.Println("")

	b := Employee{
		name:   "John",
		number: 1,
		hired:  time.Now(),
	}

	e.Boss = &b

	c := Employee{}

	d := &Employee{
		"Lucy",
		17,
		&b, // will print as pointer only
		time.Now(),
	} // we can do this way but we will have to give all values

	// also possible to o like
	t := struct {
		name   string
		number int
	}{
		name:   "test",
		number: 1,
	}
	// or
	var u = struct {
		name   string
		number int
	}{
		"test",
		1,
	}
	// but can't mix like
	// var u2 = struct{
	// 	name string
	// 	number int
	// }{
	// 	"test",
	// 	number: 1,
	// }
	// or miss values like
	// u3 := struct {
	// 	name   string
	// 	number int
	// }{
	// 	"test",
	// }

	fmt.Println("")
	fmt.Printf("E : %T %+[1]v \n", e)
	fmt.Printf("B : %T %+[1]v \n", b)
	fmt.Printf("C : %T %+[1]v \n", c)
	fmt.Printf("D : %T %+[1]v \n", d)
	fmt.Printf("T : %T %+[1]v \n", t)
	fmt.Printf("U : %T %+[1]v \n", u)

	z := Employee2{
		name:   "John",
		number: 1,
		hired:  time.Now(),
	}

	// e = z // not possible directly but we can do like bellow
	e = Employee(z) // since same structural format we can easily convert

	// also
	t = u
	// not like this
	// e = t
	// e = Employee() // also not not same structure

	p := struct {
		name   string
		number int
		Boss   *Employee
		hired  time.Time
	}{
		name:   "John",
		number: 1,
		hired:  time.Now(),
	}

	e = Employee(p) // sicne same structure we can do this

	q := struct {
		name   string
		Boss   *Employee
		number int
		hired  time.Time
	}{
		name:   "John",
		number: 1,
		hired:  time.Now(),
	}

	// e = Employee(q) // not possible as oder of fields differant

	// accessing

	q2 := &q

	fmt.Println("Field can be acceesse like q.name : ", q.name)
	fmt.Println("In case of pointers we can still do q2.name : ", q2.name)
	fmt.Println("Also (*q2).name : ", (*q2).name)
}

func StructinMap(run bool) {
	if !run {
		return
	}

	m1 := map[string]Employee{}

	m1["Lucy"] = Employee{
		name:   "Lucy",
		number: 1,
		hired:  time.Now(),
	}

	m1["John"] = Employee{
		name:   "John",
		number: 1,
		// Boss: &m1["Lucy"],  // is not possible to do as map not allow to take the addres of its elments this way
		hired: time.Now(),
	}

	// name := m1["John"].name // ok to do
	// m1["John"].number++     // same issue . we can't acces the address to do such direct operaion

	m2 := map[string]*Employee{}

	m2["Lucy"] = &Employee{
		name:   "Lucy",
		number: 1,
		hired:  time.Now(),
	}

	m2["John"] = &Employee{
		name:   "John",
		number: 1,
		Boss:   m2["Lucy"], // is ok as we now stroing the pointer/address to struct directly in map
		hired:  time.Now(),
	}

	// name := m2["John"].name // ok to do
	m2["John"].number++ // ok to do as key "John" is  pointe to struct

	fmt.Printf("m1 : %T %+[1]v \n", m1)
	fmt.Printf("m2 : %T %+[1]v \n", m2)
}

type Response struct {
	Page  int      `json:"page"`
	Words []string `json:"words,omitempty"`
	Names []string `json:"names"`
	wont  string   `json:"bar"`
}

func TagsAndJSON(run bool) {
	if !run {
		return
	}

	r1 := Response{Page: 1, Words: []string{"up", "in", "out"}, Names: []string{"Anju", "Niyatha", "Sankaran"}, wont: "not exported"}

	fmt.Printf("r1: %#v \n", r1)

	j1, err := json.Marshal(r1)

	if err != nil {
		fmt.Fprintf(os.Stderr, "json Marshal failed %s \n", err)
		os.Exit(-1)
	}

	fmt.Println("j1:", string(j1))

	var r2 Response

	err = json.Unmarshal(j1, &r2)

	if err != nil {
		fmt.Fprintf(os.Stderr, "json Unmarshal failed %s \n", err)
		os.Exit(-1)
	}

	fmt.Printf("r2: %#v \n", r2)

	r3 := Response{Page: 1}

	fmt.Printf("r2: %#v \n", r3)

	j2, err := json.Marshal(r3)

	if err != nil {
		fmt.Fprintf(os.Stderr, "json Marshal failed %s \n", err)
		os.Exit(-1)
	}

	fmt.Println("j2:", string(j2))

}
