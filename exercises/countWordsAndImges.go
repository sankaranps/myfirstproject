package exercises

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/html"
)

var raw = `
<!DOCTYPE html>
<html>
  <body>
    <h1>My First Heading</h1>
      <p>My first paragraph.</p>
      <p>HTML <a href="https://www.w3schools.com/html/html_images.asp">images</a> are defined with the img tag:</p>
      <img src="xxx.jpg" width="104" height="142">
	  <script>
		alertv("Hello, world!");
	  </script>
  </body>
</html>`

// count number of words (words in p tags ... and images)
func CountWordsAndImges(run bool) {
	if !run {
		return
	}

	// raw = GetSiteHtmlAsString("https://google.com")

	node, err := html.Parse(bytes.NewReader([]byte(raw)))

	if err != nil {
		fmt.Fprintf(os.Stderr, "parse failed %s \n", err)
		os.Exit(-1)
	}

	var words, pics int

	visit(node, &words, &pics)

	fmt.Printf("Have %d words and %d pics.\n", words, pics)
}

func visit(node *html.Node, words, pics *int) {
	// if it's a text node, get the space-delimited words;
	// if it's an element node then see what tag it has

	switch node.Type {
	case html.TextNode:
		// fmt.Println(node.Data)
		if s := strings.TrimSpace(node.Data); len(s) > 0 {
			*words += len(strings.Fields(s))
		}

	case html.ElementNode:
		// fmt.Println(node.Data)
		switch node.Data {
		case "img":
			*pics++
		case "script":
			// we can exclude script nodes being visited
			return
		}
	}

	for nc := node.FirstChild; nc != nil; nc = nc.NextSibling {
		visit(nc, words, pics)
	}
}

func GetSiteHtmlAsString(link string) string {
	res, err := http.Get(link)
	if err != nil {
		log.Fatal(err)
	}
	content, err := io.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return string(content)
}
