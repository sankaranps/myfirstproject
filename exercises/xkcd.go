package exercises

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const fileName = "comics.json"

func FetchXKCD(run bool) {
	if !run {
		return
	}

	// https://xkcd.com/557/

	switch argLen := len(os.Args); argLen {
	case 0, 1:
		close()
	case 2:
		if os.Args[1] == "fetch" {
			fetchComics()
		} else {
			close()
		}
	default:
		if os.Args[1] == "search" {
			searchComics(os.Args[2:])
		} else {
			close()
		}
	}
}

func close() {
	fmt.Println("Need atlest one argument, search or fetch")
	os.Exit(0)
}

func searchComics(s []string) {
	// fmt.Println(s)
	if len(s) < 1 {
		fmt.Fprintln(os.Stderr, "No search terms")
		os.Exit(-1)
	}

	// get fiel content to struct
	type xkcd struct {
		Num        int    `json:"num"`
		Day        string `json:"day"`
		Month      string `json:"month"`
		Year       string `json:"year"`
		Title      string `json:"title"`
		Transcript string `json:"transcript"`
	}

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to open file: %s\n", err.Error())
		os.Exit(-1)
	}

	var (
		items []xkcd
		terms []string
		count int
	)

	for _, t := range s {
		terms = append(terms, strings.ToLower(t))
	}

	err = json.NewDecoder(file).Decode(&items)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to read file data : %s\n", err.Error())
		os.Exit(-1)
	}

outerLoop:
	for i := range items {
		title := strings.ToLower(items[i].Title)
		transcript := strings.ToLower(items[i].Transcript)

		for _, term := range terms {
			if !strings.Contains(title, term) && !strings.Contains(transcript, term) {
				continue outerLoop
			}
		}

		fmt.Printf(
			"https://xkcd.com/%d/ %s/%s/%s %s\n",
			items[i].Num, items[i].Day, items[i].Month, items[i].Year, items[i].Title,
		)

		count++
	}

	fmt.Printf("Total: %d, found %d", len(items), count)
}

func fetchComics() {
	const limit = 100
	var (
		fails   int
		count   int
		storage io.WriteCloser = os.Stdout
	)

	storage, err := os.Create(fileName)
	if err != nil {
		fmt.Printf("Failed to create file : %s\n", err.Error())
		fails++
	}

	fmt.Fprint(storage, "[")       // write [ to output first
	defer fmt.Fprint(storage, "]") // write later ] to stoaree

	for i := 1; i <= limit; i++ {
		data, err := getMetadata(i)

		if err != nil {
			fmt.Printf("%d failed due to : %s\n", i, err.Error())
			fails++
			continue
		}

		if count > 0 {
			fmt.Fprint(storage, ",") // add a coma if it not the first item
		}

		// writeToFile(fileName, data)
		_, err = io.Copy(storage, bytes.NewBuffer(data))
		if err != nil {
			fmt.Fprintf(os.Stderr, "stopped : %s\n", err.Error())
			os.Exit(-1)
		}

		count++
	}

	fmt.Printf("Fetched: %d, Failed %d\n", count, fails)
}

// func writeToFile(fileName string, meta []byte) error {
// 	fmt.Println("Mesata data :", string(meta), fileName)
// 	return nil
// }

// func createFile(filePath string) {
// 	if _, err := os.Stat(filePath); err != nil {
// 		// File exists
// 		fmt.Println("Creating file")
// 		file, err := os.Create(filePath)

// 		if err != nil {
// 			return nil
// 		}

// 	}
// }

func getMetadata(id int) ([]byte, error) {
	url := fmt.Sprintf("https://xkcd.com/%d/info.0.json", id)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			return nil, err
		}

		return body, nil
	}

	return nil, fmt.Errorf("Unable to get meta data for %d, http status: %s", id, resp.Status)
}
