package functions

import "fmt"

func ParameterPassing(run bool) {
	if !run {
		return
	}

	fmt.Println("=== Passsing array ===")
	a := [3]int{1, 2, 3}
	fmt.Println("a before function call : ", a)
	fmt.Printf("Pointer to a : %p\n", &a)
	b := passingArray(a)
	fmt.Println("a and b after function call : ", a, b) // a not changed

	fmt.Println()
	fmt.Println("=== Passsing Slice ===")
	s := []int{1, 2, 3}
	fmt.Println("s before function call : ", s)
	fmt.Printf("Pointer to s : %p\n", s)
	passingSlice(s)
	fmt.Println("s after function call : ", s) // s changed
	fmt.Println("Both slice pointer points to same array in memeory")

	fmt.Println()
	fmt.Println("=== Passsing slice (make example) ===")
	t := []int{1, 2, 3}
	fmt.Println("t before function call : ", t)
	fmt.Printf("Pointer to t : %p\n", t)
	passSlice2(t)
	fmt.Println("t after function call : ", t) // t not changed
	fmt.Println("Both slice initially pointed to same array but make changed it.")

	fmt.Println()
	fmt.Println("=== Passsing slice pointer ===")
	u := []int{1, 2, 3}
	fmt.Println("u before function call : ", u)
	fmt.Printf("Pointer to u : %p\n", u)
	passSlicePointer(&u)
	fmt.Println("u after function call : ", u) // u changed
	fmt.Println("On passing pointer both will be changed.")

	fmt.Println()
	fmt.Println("=== Passsing Map ===")
	m := map[int]int{0: 1, 1: 2, 2: 3}
	fmt.Println("m before function call : ", m)
	fmt.Printf("Pointer to m : %p\n", m)
	passingMap(m)
	fmt.Println("m after function call : ", m) // s changed
	fmt.Println("Both maps pointer points to same array in memeory")

	fmt.Println()
	fmt.Println("=== Passsing map (make example) ===")
	n := map[int]int{0: 1, 1: 2, 2: 3}
	fmt.Println("t before function call : ", n)
	fmt.Printf("Pointer to n : %p\n", n)
	passMap2(n)
	fmt.Println("t after function call : ", n) // n not changed fully
	fmt.Println("Both maps initially pointed to same array but make changed it.")

	fmt.Println()
	fmt.Println("=== Passsing map pointer ===")
	o := map[int]int{0: 1, 1: 2, 2: 3}
	fmt.Println("u before function call : ", o)
	fmt.Printf("Pointer to o : %p\n", o)
	passMapPointer(&o)
	fmt.Println("u after function call : ", o) // o changed
	fmt.Println("On passing pointer both will be changed.")
}

func passingArray(p [3]int) int {
	fmt.Printf("Pointer to p : %p\n", &p)
	p[1] = 5
	return p[1]
}

func passingSlice(p1 []int) {
	fmt.Printf("Pointer to p1 : %p\n", p1)
	p1[1] = 6
}

func passSlice2(p2 []int) {
	fmt.Printf("Pointer to p2 before make : %p\n", p2)
	p2[0] = 6
	p2 = make([]int, 3)
	p2[0] = 9
	fmt.Printf("Pointer to p2 after make : %p\n", p2)
}

func passSlicePointer(p3 *[]int) {
	fmt.Printf("Pointer to p3 before make : %p\n", *p3)
	(*p3)[0] = 6
	*p3 = make([]int, 3)
	(*p3)[0] = 9
	fmt.Printf("Pointer to p3 after make : %p\n", *p3)
}

func passingMap(m1 map[int]int) {
	fmt.Printf("Pointer to m1 : %p\n", m1)
	m1[1] = 6
}

func passMap2(m2 map[int]int) {
	fmt.Printf("Pointer to m2 before make : %p\n", m2)
	m2[0] = 6
	m2 = make(map[int]int, 3)
	m2[0] = 9
	fmt.Printf("Pointer to m2 after make : %p\n", m2)
}

func passMapPointer(m3 *map[int]int) {
	fmt.Printf("Pointer to m3 before make : %p\n", *m3)
	(*m3)[0] = 6
	*m3 = make(map[int]int, 3)
	(*m3)[0] = 9
	fmt.Printf("Pointer to m3 after make : %p\n", *m3)
}
