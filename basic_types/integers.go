package basicTypes

import (
	"fmt"
	"os"
)

func Average(run bool) {
	if run {
		var sum float64
		var n int
		for {
			var val float64

			if _, err := fmt.Fscanln(os.Stdin, &val); err != nil {
				break
			}

			sum += val
			n++
		}

		if n == 0 {
			fmt.Fprintln(os.Stderr, "no value")
			os.Exit(-1)
		}

		fmt.Println("The avarage is: ", sum/float64(n))
	}

}
