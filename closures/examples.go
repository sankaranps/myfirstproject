package closures

import "fmt"

func FibonacciSeries(limit int) {
	fn := fib

	f := fn()

	for i := f(); i < limit; i = f() {
		fmt.Println(i)
	}
}

func fib() func() int {
	a, b := 0, 1

	return func() int {
		a, b = b, a+b

		return b
	}
}

func FibonacciSeriesMulty(run bool) {
	if !run {
		return
	}

	f, g := fib(), fib()

	fmt.Println(f(), f(), f(), f(), f()) // 1 2 3 5 8
	fmt.Println(g(), g(), g(), g(), g()) // 1 2 3 5 8

	fn := fib()

	h, i := fn, fn

	fmt.Println()
	fmt.Println(h(), h(), h(), h(), h()) // 1 2 3 5 8
	fmt.Println(i(), i(), i(), i(), i()) // 13 21 34 55 89
}

func do(f func()) {
	f()
}

func Looping(run bool) {
	if !run {
		return
	}

	for i := 0; i < 5; i++ {
		f := func() {
			fmt.Printf("%d @ %p\n", i, &i) // value will change as loop runs but sicne it called immeditely we will get 1, 2 ,3 ..
		}

		do(f)
	}
}

func LoopingBug(run bool) {
	if !run {
		return
	}

	count := 5
	s := make([]func(), count)

	for i := 0; i < count; i++ {
		// to avoid this we can use lile
		// i := i (this will force i to pint to new adress location and value won't get overwritten)
		s[i] = func() {
			fmt.Printf("%d @ %p\n", i, &i) // value and address will eb the same always, value = 5 as at end of loop sicne i [opoitns to same adress it will eb overwriten to 5 befoe fucntion called
		}
	}

	for j := 0; j < count; j++ {
		do(s[j])
	}
}
