package searchregex

import (
	"fmt"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

func BasicReachAndReplace(run bool) {
	if !run {
		return
	}

	test := "Here is $1 which is $2"
	fmt.Println("Before replacing : ", test)

	test = strings.ReplaceAll(test, "$1", "honey")
	test = strings.ReplaceAll(test, "$2", "tasty")

	fmt.Println("After replacing : ", test)
}

func PrintFileCalling(run bool) {
	if !run {
		return
	}

	_, file, line, _ := runtime.Caller(1)

	idx := strings.LastIndexByte(file, '/')

	s := "=> " + file[idx+1:] + ":" + strconv.Itoa(line)

	fmt.Println(s)
}

func BasicRegex(run bool) {
	if !run {
		return
	}

	te := "abc abbc abbbc"

	re := regexp.MustCompile("b+")

	fmt.Println("Original string : ", te)
	fmt.Println("Regular expression : ", re.String())

	mm := re.FindAllString(te, -1)
	id := re.FindAllStringIndex(te, -1)

	fmt.Println("Matches : ", mm)
	fmt.Println("Match indes : ", id)

	fmt.Println("Repalce with d : ", re.ReplaceAllString(te, "d"))
	fmt.Println("Repalce with B : ", re.ReplaceAllStringFunc(te, strings.ToUpper))
}

func UUIDCheck(run bool) {
	if !run {
		return
	}

	var uuids = []string{
		"cf3ebb5f-f0bd-4662-a9a2-74680cfc656f",
		"93c0609c-d2fd-4682-a5d5-bd5988131363",
		"3edc9170-d1ee-4e78-87ac-be71d7045073",
		"3edc9170-d1ee-4e78-87ac-be71d7045073s",
		"93c0609c-d2fd-4682-a5d5-bd598813136",
		"cf3ebb5f-f0bd-6662-a9a2-74680cfc656f",
		"cf3ebb5f-f0bd-6662-79a2-74680cfc656f",
	}

	regexStr := `^[[:xdigit:]]{8}-[[:xdigit:]]{4}-[1-5][[:xdigit:]]{3}-[89abAB][[:xdigit:]]{3}-[[:xdigit:]]{12}$`

	regex := regexp.MustCompile(regexStr)

	for i, id := range uuids {
		if !regex.MatchString(id) {
			fmt.Println(i, id, "\t fails")
		}

	}
}
