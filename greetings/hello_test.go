package greetings

import (
	"testing"
)

func TestSay(t *testing.T) {

	tests := []struct {
		items  []string
		result string
	}{
		{
			result: "Hello, World!",
		},
		{
			items:  []string{"test"},
			result: "Hello, test!",
		},
		{
			items:  []string{"Niyatha", "Nora"},
			result: "Hello, Niyatha, Nora!",
		},
		{
			items:  []string{"Niyatha", "Anju", "Sankaran"},
			result: "Hello, Niyatha, Anju, Sankaran!",
		},
	}

	for i := range tests {
		if s := Say(tests[i].items); s != tests[i].result {
			t.Errorf("Wanted %s, got %s", tests[i].result, s)
		}
	}
}
