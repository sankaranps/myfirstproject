package greetings

import (
	"fmt"
	"os"
	"strings"
)

func Greet(run bool) {
	if run {
		fmt.Println(Say(os.Args[1:]))
	}
}

func Say(names []string) string {

	if len(names) < 1 {
		names = []string{"World"}
	}

	return "Hello, " + strings.Join(names, ", ") + "!"
}
