package main

import (
	basicTypes "hello/basic_types"
	"hello/closures"
	compositetypes "hello/composite_types"
	"hello/exercises"
	"hello/functions"
	"hello/goroutines"
	"hello/greetings"
	inputoutput "hello/input_output"
	"hello/networking"
	referanceandvalue "hello/referance_and_value"
	searchregex "hello/search_regex"
)

func main() {
	greetings.Greet(false)

	basicTypes.Average(false)

	compositetypes.PrintUtf8(false)
	compositetypes.WordReplace(false)

	compositetypes.Arrybasics(false)

	compositetypes.SliceBasics(false)
	compositetypes.SliceOfString(false)
	compositetypes.SliceWTF(false)

	compositetypes.BasicMaps(false)
	compositetypes.MapTest(false)
	compositetypes.DifferantMaps(false)

	compositetypes.BasicStruct(false)
	compositetypes.StructinMap(false)
	compositetypes.TagsAndJSON(false)

	inputoutput.FormatCodes(false)
	inputoutput.Cat(false)
	inputoutput.WC(false)

	functions.ParameterPassing(false)

	closures.FibonacciSeries(0) // give 100 to get Fibonacci Series
	closures.FibonacciSeriesMulty(false)
	closures.Looping(false)
	closures.LoopingBug(false)

	exercises.CountWordsAndImges(false)

	searchregex.BasicReachAndReplace(false)
	searchregex.PrintFileCalling(false)
	searchregex.BasicRegex(false)
	searchregex.UUIDCheck(false)

	referanceandvalue.PointerToSliceElmentIssues(false)
	referanceandvalue.LoopValueUsageGotchas(false)
	referanceandvalue.PointerInLoop(false)

	networking.BasicServer(false)
	networking.TodoServer(false)
	networking.BasicClient(false)
	networking.TodoClient(false)

	exercises.FetchXKCD(false)

	goroutines.CheckSiteURLS(false)
	goroutines.HttpRequestCounter(false)
	goroutines.PrimeNumber(true)

}
