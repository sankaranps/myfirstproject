package goroutines

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

type result struct {
	url     string
	err     error
	latancy time.Duration
}

func get(url string, ch chan<- result) {
	start := time.Now()

	if resp, err := http.Get(url); err != nil {
		ch <- result{err: err}
	} else {
		t := time.Since(start).Round(time.Microsecond)
		ch <- result{url, nil, t}

		resp.Body.Close()
	}
}

func CheckSiteURLS(run bool) {
	if !run {
		return
	}

	results := make(chan result)

	list := []string{
		"http://amazone.com",
		"http://google.com",
		"http://example.com",
		"http://googsdsdsle.com",
	}

	for _, url := range list {
		go get(url, results)
	}

	for range list {
		r := <-results

		if r.err != nil {
			log.Printf("%-20s %s\n", r.url, r.err)
		} else {
			log.Printf("%-20s %s\n", r.url, r.latancy)
		}
	}
}

type nextCH chan int

func (ch nextCH) handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>You got %d<h1>", <-ch)
}

// this way helps us to incrment counter each tiem with out having race condition
func counter(ch chan<- int) {
	for i := 0; ; i++ {
		ch <- i
	}
}

func HttpRequestCounter(run bool) {
	if !run {
		return
	}

	var nextID nextCH = make(chan int)

	go counter(nextID)

	http.HandleFunc("/", nextID.handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
