package inputoutput

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func Cat(run bool) {
	if !run {
		return
	}

	files := os.Args[1:]

	for i := range files {

		file, err := os.Open(files[i])

		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue // continew with rest of the files
		}

		if _, err := io.Copy(os.Stdout, file); err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue // continew with rest of the files
		}

		file.Close()
	}
}

func WC(run bool) {
	if !run {
		return
	}

	files := os.Args[1:]

	for i := range files {

		var lc, wc, cc int

		file, err := os.Open(files[i])

		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue // continew with rest of the files
		}

		scanner := bufio.NewScanner(file)

		for scanner.Scan() {
			line := scanner.Text()

			lc++
			cc += len(line)
			wc += len(strings.Fields(line))
		}

		fmt.Printf("%7d %7d %7d %s\n", lc, wc, cc, files[i])

		file.Close()
	}

}
