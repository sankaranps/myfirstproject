package inputoutput

import "fmt"

func FormatCodes(run bool) {
	if !run {
		return
	}

	row := 0
	numberFormatCodes(&row)
	sliceFormatCodes(&row)
	arryFormatCodes(&row)
	mapFormatCodes(&row)
	stringFormatCodes(&row)
}

func numberFormatCodes(row *int) {
	*row++
	fmt.Printf("%02d. Integers\n", *row)
	fmt.Println("============")

	a, b := 12, 345

	fmt.Println()
	fmt.Println("Decimal notation (base 10)")
	fmt.Println("--------------------------")
	fmt.Println("a | b")
	fmt.Printf("%d %d\n", a, b)
	fmt.Printf("%4d %4d\n", a, b)
	fmt.Printf("%8d %8d\n", a, b)
	fmt.Println()
	fmt.Println("|a|b|")
	fmt.Printf("|%2d|%2d|\n", a, b)   // atleast 2 apce and right aligned (345 need 3 space so will expand)
	fmt.Printf("|%4d|%4d|\n", a, b)   // atleast 4 apce and right aligned
	fmt.Printf("|%6d|%6d|\n", a, b)   // atleast 6 apce and right aligned
	fmt.Printf("|%06d|%06d|\n", a, b) // space filled
	fmt.Printf("|%-6d|%-6d|\n", a, b) // left aligned
	fmt.Println()

	fmt.Println("Hexa Decimal notation (base 16)")
	fmt.Println("-------------------------------")
	fmt.Println("a | b")
	fmt.Printf("%x %x\n", a, b)
	fmt.Printf("%#x %#x\n", a, b)
	fmt.Printf("%X %X\n", a, b)
	fmt.Printf("%#X %#X\n", a, b)

	*row++
	fmt.Println()
	fmt.Printf("%02d. Floating point\n", *row)
	fmt.Println("==================")

	var (
		c, d = 1.2, 3.34
	)

	fmt.Println()
	fmt.Println("Decimal notation (base 10)")
	fmt.Println("--------------------------")
	fmt.Println("c | d")
	fmt.Printf("%f | %f\n", c, d)     // shows 6 decimal part by default
	fmt.Printf("%.2f | %.2f\n", c, d) // .2 = show 2 decimal part
	fmt.Printf("%.3f | %.3f\n", c, d)

	fmt.Println()
	fmt.Println("|c | d|")
	fmt.Printf("|%6.3f|%6.3f|\n", c, d)   // 6 = atlest 6 space between | and 3 decimal parts
	fmt.Printf("|%6f|%6f|\n", c, d)       // 6 = atlest 6 space between | but number won'tfit in that space
	fmt.Printf("|%6.2f|%6.2f|\n", c, d)   // right aligned
	fmt.Printf("|%06.2f|%06.2f|\n", c, d) // space filled
	fmt.Printf("|%-6.2f|%-6.2f|\n", c, d) // left aligned
	fmt.Println()
}

func sliceFormatCodes(row *int) {
	*row++
	fmt.Printf("%02d. Slice formatters\n", *row)
	fmt.Println("====================")

	s := []int{1, 2, 3}

	fmt.Println()
	fmt.Printf("%T \n", s)  // type
	fmt.Printf("%v \n", s)  // value
	fmt.Printf("%#v \n", s) // both type & value
	fmt.Println()

	// c := []rune{"a", "b", "c"} // strigs are not allowed in rune (sicne can not convert to bytes)
	// d := []bytes{"a", "b", "c"} // strigs are not allowed in bytes (sicne can not convert to bytes)
}

func arryFormatCodes(row *int) {
	*row++
	fmt.Printf("%02d. Array formatters\n", *row)
	fmt.Println("====================")

	a := [3]rune{1, 2, 3}

	fmt.Println()
	fmt.Println("Array of Rune (of integers)")
	fmt.Println("------------------------=--")
	fmt.Printf("%T \n", a)  // type
	fmt.Printf("%v \n", a)  // value
	fmt.Printf("%#v \n", a) // both type & value
	fmt.Println()

	b := [3]rune{'a', 'b', 'c'} // charactor rune '' is for charctor "" string

	fmt.Println()
	fmt.Println("Array of Rune (of charctors)")
	fmt.Println("----------------------------")
	fmt.Printf("%T \n", b)  // type
	fmt.Printf("%v \n", b)  // integer value of charactors
	fmt.Printf("%q \n", b)  // charactor reperesesntaion (q is for string)
	fmt.Printf("%#v \n", b) // both type & value
	fmt.Printf("%#q \n", b) // un-foctunately we cant do this as charctor type and charctor this way
	fmt.Println()

	// c := [3]rune{"a", "b", "c"} // strigs are not allowed in rune (sicne can not convert to bytes)
	// d := [3]bytes{"a", "b", "c"} // strigs are not allowed in bytes (sicne can not convert to bytes)
}

func mapFormatCodes(row *int) {
	*row++
	fmt.Printf("%02d. Map formatters\n", *row)
	fmt.Println("==================")

	m := map[string]int{"and": 1, "test": 2, "base": 3}

	fmt.Println()
	fmt.Printf("%T \n", m)  // type
	fmt.Printf("%v \n", m)  // value
	fmt.Printf("%#v \n", m) // both type & value
	fmt.Println()
}

func stringFormatCodes(row *int) {
	*row++
	fmt.Printf("%02d. String formatters\n", *row)
	fmt.Println("=====================")

	s := "a string"

	fmt.Println()
	fmt.Println("Normal String")
	fmt.Println("-------------")
	fmt.Printf("%T \n", s)  // type
	fmt.Printf("%v \n", s)  // value
	fmt.Printf("%q \n", s)  // value in real world
	fmt.Printf("%#v \n", s) // both type & value

	// byte of strings
	b := []byte(s)

	fmt.Println()
	fmt.Println("Bytes slice representing String")
	fmt.Println("-------------------------------")
	fmt.Printf("%T \n", b)         // type
	fmt.Printf("%v \n", b)         // value
	fmt.Printf("%q \n", b)         // value in real world
	fmt.Printf("%#v \n", b)        // both type & value
	fmt.Printf("%v \n", string(b)) // cast it to actual string
	fmt.Println()
}
