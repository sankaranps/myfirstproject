package referanceandvalue

import "fmt"

type user struct {
	name  string
	count int
}

func addTo(u *user) { u.count++ }

func PointerToSliceElmentIssues(run bool) {
	if !run {
		return
	}

	users := []user{{"Alice", 0}, {"Bob", 0}}

	alice := &users[0]
	amy := user{name: "amy", count: 1}

	users = append(users, amy) // here users will be re allocated with new array and emem
	// this will make alice pointing to a dangling value im meme

	addTo(alice)

	fmt.Println(users)
	fmt.Println(alice)
}

func LoopValueUsageGotchas(run bool) {
	if !run {
		return
	}

	items := [][2]byte{{1, 2}, {3, 4}, {5, 6}} // slcie of 2 array byte
	fmt.Println("items : ", items)

	a := [][]byte{} // we want to make items to slice of slcie of bytes
	b := [][]byte{} // we want to make items to slice of slcie of bytes

	for _, item := range items {
		// item copy of 2 byte array ane each tiem it will eb copied in same location in mem
		a = append(a, item[:]) // item[:] is slice and get refrance to item

		// to acorretc this we can do copy
		i := make([]byte, len(item))
		// i := make([]byte, 0, len(item))  // wont work as copy uses lengrth of i, so no legth to copy items
		copy(i, item[:])

		b = append(b, i)
	}

	fmt.Println("items a : ", a)
	fmt.Println("items b : ", b)
}

type change struct {
	name string
}

type changeResolver struct {
	resolver *change
}

func changeresolvers() (aa []changeResolver, bb []changeResolver) {
	changes := []change{{"One"}, {"two"}, {"three"}, {"four"}}

	for _, change := range changes {
		// will cause all resolvers to point to same change
		// as change is a copy and it get overwqritten each tie in loop
		aa = append(aa, changeResolver{&change})
		// to avoid it we have to do a copyig here like
		b := change
		bb = append(bb, changeResolver{&b})
	}

	return aa, bb
}

func PointerInLoop(run bool) {
	if !run {
		return
	}

	aa, bb := changeresolvers()

	for _, a := range aa {
		fmt.Printf(" a = %#v\n", a)
	}

	for _, b := range bb {
		fmt.Printf(" b = %#v\n", b)
	}
}
