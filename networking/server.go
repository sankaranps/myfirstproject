package networking

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func BasicServer(run bool) {
	if !run {
		return
	}

	http.HandleFunc("/", helloWorldHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func helloWorldHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!! from %s \n", r.URL.Path[1:])
}

func TodoServer(run bool) {
	if !run {
		return
	}

	http.HandleFunc("/", todoHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

var form = `
<!DOCTYPE html>
<html>
<head>
    <title>TODO</title>
</head>
<body>
    <h1>Todo #{{.ID}}</h1>
    <div>{{printf "User %d" .UserID}}</div>
    <div>{{printf "%s (completed: %t)" .Title .Completed }}</div>
</body>
</html>`

func todoHandler(wr http.ResponseWriter, r *http.Request) {
	var todo struct {
		ID        int    `json:"id"`
		Title     string `json:"title"`
		UserID    int    `json:"userId"`
		Completed bool   `json:"completed"`
	}

	uri := "todos/1"
	if len(r.URL.Path) > 1 {
		uri = r.URL.Path[1:]
	}

	println(r.URL.Path)

	c := &client{}

	err := c.JSON(url+uri, &todo)
	if err != nil {
		http.Error(wr, err.Error(), http.StatusServiceUnavailable)
		return
	}

	tmpl := template.New("mine")

	tmpl, err = tmpl.Parse(form)
	if err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(wr, todo)
	if err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		return
	}
}
