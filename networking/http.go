package networking

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type client struct{}

func (c *client) Get(url string, body *[]byte) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		*body, err = ioutil.ReadAll(resp.Body)

		if err != nil {
			return err
		}
	}

	return nil
}

func (c *client) JSON(url string, data any) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		err = json.NewDecoder(resp.Body).Decode(data)

		if err != nil {
			return err
		}
	}

	return nil
}
