package networking

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func clients(url string, body *[]byte) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		*body, err = ioutil.ReadAll(resp.Body)

		if err != nil {
			return err
		}
	}

	return nil
}

func BasicClient(run bool) {
	if !run {
		return
	}

	uri := "test"

	if len(os.Args) > 1 {
		uri = os.Args[1]
	}

	var body []byte

	c := &client{}

	err := c.Get("http://localhost:8080/"+uri, &body)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}

	fmt.Println("response : ", string(body))
}

const url = "https://jsonplaceholder.typicode.com/"

func TodoClient(run bool) {
	if !run {
		return
	}

	var todo struct {
		Id        int    `json:"id"`
		Title     string `json:"title"`
		Completed bool   `json:"completed"`
	}

	uri := "todos/1"

	var body []byte

	c := &client{}

	err := c.Get(url+uri, &body)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}

	err = json.Unmarshal(body, &todo)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}

	fmt.Printf("%#v\n", todo)
}
